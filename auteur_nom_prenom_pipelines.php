<?php
/**
 * Utilisations de pipelines par Nom et prénom des auteurs
 *
 * @plugin     Nom et prénom des auteurs
 * @copyright  2019
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Auteur_nom_prenom\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function auteur_nom_prenom_affiche_milieu($flux) {
	if ($flux['args']['exec'] == "auteur_edit") {
		$texte = recuperer_fond(
			'prive/fonds/auteur_edit', array(
				'ordre'=>lire_config('auteur_nom_prenom/ordre'),
				'civilite'=>lire_config('auteur_nom_prenom/civilite'),
			)
		);
		if (($p = strpos($flux['data'], '<!--affiche_milieu-->')) !== false) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

function auteur_nom_prenom_formulaire_verifier($flux) {
	if ($flux['args']['form'] == "editer_auteur") {
		if (!(trim(_request('auteur_nom'))) && !(trim(_request('auteur_prenom')))) {
			$flux['data']['auteur_nom'] = _T('auteur_nom_prenom:au_moins_un_champ');
			$flux['data']['auteur_prenom'] = _T('auteur_nom_prenom:au_moins_un_champ');
			unset($flux['data']['nom']);
		}
	}
	return $flux;
}

function auteur_nom_prenom_post_edition($flux) {

	if (isset($flux['args']['table']) && $flux['args']['table'] == 'spip_auteurs') {
		$nom = '';
		if (trim(_request('auteur_prenom')) || trim(_request('auteur_nom'))) {
			// si on a saisi les champs extra auteur_prenom ou auteur_nom, renseigner le nom
			$auteur_prenom = trim(_request('auteur_prenom'));
			$auteur_nom = trim(_request('auteur_nom'));
		} else {
			if (trim($flux['data']['auteur_prenom']) || trim($flux['data']['auteur_nom'])) {
				// sinon ils sont peut être dans le contexte
				$auteur_prenom = trim($flux['data']['auteur_prenom']);
				$auteur_nom = trim($flux['data']['auteur_nom']);
			} else {
				// sinon on les lit dans la table
				$infos_auteur = sql_fetsel(
					'auteur_prenom, auteur_nom, nom',
					'spip_auteurs',
					'id_auteur = ' . intval($flux['args']['id_objet'])
				);
				$auteur_prenom = trim($infos_auteur['auteur_prenom']);
				$auteur_nom = trim($infos_auteur['auteur_nom']);
			}
		}
		if (trim($auteur_prenom) || trim($auteur_nom)) {
			if(lire_config('auteur_nom_prenom/ordre')=='prenom') {
				$nom = $auteur_prenom . ' ' . $auteur_nom;
			} else {
				$nom = $auteur_nom . ' ' . $auteur_prenom;
			}
		}
		if (trim($nom)) {
			sql_updateq('spip_auteurs', array('nom' => $nom), 'id_auteur = ' . $flux['args']['id_objet']);
		}
	}

	return $flux;
}

function auteur_nom_prenom_champs_extras() {
	$champs = array();
	$champs["spip_auteurs"]['auteur_civilite'] = array(
		'options' =>
			array(
				'nom'        => 'auteur_civilite',
				'label'      => _T('auteur_nom_prenom:auteur_civilite'),
				'sql'        => 'varchar(3) NULL DEFAULT NULL',
				'versionner' => 'on',
				'data'      => array(
					'M'   => 'M.',
					'Mme' => 'Mme',
				),
			),
		'saisie'  => 'radio',
	);
	$champs["spip_auteurs"]['auteur_nom'] = array(
		'options' =>
			array(
				'nom'        => 'auteur_nom',
				'label'      => _T('auteur_nom_prenom:auteur_nom'),
				'sql'        => 'text NULL DEFAULT NULL',
				'versionner' => 'on',
			),
		'saisie'  => 'input',
	);
	$champs["spip_auteurs"]['auteur_prenom'] = array(
		'options' =>
			array(
				'nom'        => 'auteur_prenom',
				'label'      => _T('auteur_nom_prenom:auteur_prenom'),
				'sql'        => 'text NULL DEFAULT NULL',
				'versionner' => 'on',
			),
		'saisie'  => 'input',
	);
	return $champs;
}

function auteur_nom_prenom_declarer_champs_extras($champs = array()) {
	return array_merge($champs, auteur_nom_prenom_champs_extras());
}
function auteur_nom_prenom_declarer_champs_extras_install() {
	return auteur_nom_prenom_champs_extras();
}
