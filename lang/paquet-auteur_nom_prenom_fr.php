<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[ $GLOBALS['idx_lang'] ] = array(

	// A
	'auteur_nom_prenom_description' => '',
	'auteur_nom_prenom_nom'         => 'Nom et prénom des auteurs',
	'auteur_nom_prenom_slogan'      => '',

);
