<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[ $GLOBALS['idx_lang'] ] = array(

	// A
	'au_moins_un_champ'       => 'Merci de renseigner un des deux champs',
	'auteur_civilite'         => 'Civilité',
	'auteur_nom'              => 'Nom',
	'auteur_prenom'           => 'Prénom',
	'auteur_nom_prenom_titre' => 'Nom et prénom des auteurs',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',

	// O
	'ordre_champs' => 'Ordre',
	'ordre_champs_explication' => 'Dans quel ordre afficher les champs ?',
	'ordre_nom' => 'Nom Prénom',
	'ordre_prenom' => 'Prénom Nom',

	// T
	'titre_page_configurer_auteur_nom_prenom' => 'Configuration',
	
	// U
	'utiliser_civilite' => 'Utiliser le choix de la civilité',
	
);
