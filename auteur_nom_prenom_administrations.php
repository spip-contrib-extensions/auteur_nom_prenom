<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Nom et prénom des auteurs
 *
 * @plugin     Nom et prénom des auteurs
 * @copyright  2019
 * @author     nicod_
 * @licence    GNU/GPL
 * @package    SPIP\Auteur_nom_prenom\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('base/upgrade');
include_spip('inc/cextras');
include_spip('auteur_nom_prenom_pipelines');

/**
 * Fonction d'installation et de mise à jour du plugin Nom et prénom des auteurs.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 *
 * @return void
 **/
function auteur_nom_prenom_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	cextras_api_upgrade(auteur_nom_prenom_declarer_champs_extras_install(), $maj['create']);
	cextras_api_upgrade(auteur_nom_prenom_declarer_champs_extras_install(), $maj['1.1.0']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

	if(!lire_config('auteur_nom_prenom/ordre')) {
		ecrire_config('auteur_nom_prenom/ordre', 'prenom');
	}
}

/**
 * Fonction de désinstallation du plugin Nom et prénom des auteurs.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 *
 * @return void
 **/
function auteur_nom_prenom_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(auteur_nom_prenom_declarer_champs_extras_install());
	effacer_meta($nom_meta_base_version);
}
