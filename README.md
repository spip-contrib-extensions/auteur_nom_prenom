# Nom et prénom des auteurs

Ajoute les champs extra auteur_nom, auteur_prenom et auteur_civilite sur les auteurs.

Sur la page ?exec=auteur_edit, le champ nom est désactivé au profit des champs nom et prénom, qui sont déplacés juste au dessus en jQuery.  
Le champ nom est automatiquement rempli avec le nom et le prénom à chaque modification (depuis le pipeline post_edition).

La page de configuration propose de gérer la civilité (oui par défaut), et l'ordre dans lequel remplir le champ nom (Prénom Nom, par défaut, ou Nom Prénom)